package com.example.ravia.oneassisthackathon.Controllers;


import com.example.ravia.oneassisthackathon.remote.APIService;
import com.example.ravia.oneassisthackathon.remote.RetrofitClient;

/**
 * Created by ravia on 11/11/2017.
 */

public class ApiUtils {
    public static final String BASE_URL = "http://sharkups.com:8080/eddviss/";

    public static APIService getApiService() {
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
