package com.example.ravia.oneassisthackathon.Controllers;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.example.ravia.oneassisthackathon.Constants.Constants;
import com.example.ravia.oneassisthackathon.Utils.CommonUtility;
import com.example.ravia.oneassisthackathon.Utils.PreferenceUtils;
import com.example.ravia.oneassisthackathon.remote.APIService;
import com.example.ravia.oneassisthackathon.volley.LruBitmapCache;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ravi Abey Paul on 4/27/15.
 */
public class AppController extends MultiDexApplication {


    public static final String TAG = AppController.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    String curCourseId;
    private static AppController mInstance;
    LruBitmapCache mLruBitmapCache;
    CommonUtility cUtils;
    Gson gson;
    PreferenceUtils pUtils;
    APIService mService;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        gson = new Gson();
        pUtils = new PreferenceUtils(getApplicationContext(), Constants.shdPrefName);
        cUtils = new CommonUtility();
        mService = ApiUtils.getApiService();
    }

    public CommonUtility getcUtils() {
        if(cUtils!=null)
            return cUtils;
        else
            return cUtils = new CommonUtility();
    }

    public void setcUtils(CommonUtility cUtils) {
        this.cUtils = cUtils;
    }



    public PreferenceUtils getpUtils() {
        if(pUtils!=null)
            return pUtils;
        else
            return pUtils = new PreferenceUtils(getApplicationContext(),Constants.shdPrefName);
    }

    public void setpUtils(PreferenceUtils pUtils) {
        this.pUtils = pUtils;
    }


    private static boolean activityVisible;


    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }



    public Gson getGson() {
        if(gson!=null)
            return gson;
        else
            return gson = new Gson();
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }


    public String getCurCourseId() {
        return curCourseId;
    }

    public void setCurCourseId(String curCourseId) {
        this.curCourseId = curCourseId;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public APIService getmService() {
        return mService;
    }

    public void setmService(APIService mService) {
        this.mService = mService;
    }



    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public LruBitmapCache getLruBitmapCache() {
        if (mLruBitmapCache == null)
            mLruBitmapCache = new LruBitmapCache();
        return this.mLruBitmapCache;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}