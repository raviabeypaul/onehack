package com.example.ravia.oneassisthackathon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravia.oneassisthackathon.Models.Data;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;

public class DataList extends AppCompatActivity {

    LinearLayoutManager llManager;
    ProductAdapter productAdapter;
    ArrayList<Data> datas= new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_list);
        ButterKnife.bind(this);

    }

    public void initData()
    {
        Data d1 = new Data();
        d1.setAvg_Rating("7");
        d1.setCost("25000");
        d1.setProduct_model("D101");
        d1.setStabilityOfProduct("5");
        d1.setWarrantyServices("7");
        d1.setBrandName("LG");
        d1.setLifeOfProduct("8");
        d1.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d2 = new Data();
        d2.setAvg_Rating("8");
        d2.setCost("15000");
        d2.setProduct_model("D102");
        d2.setStabilityOfProduct("7");
        d2.setWarrantyServices("8");
        d2.setBrandName("LG");
        d2.setLifeOfProduct("14");

        d2.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d3 = new Data();
        d3.setAvg_Rating("6");
        d3.setCost("18000");
        d3.setProduct_model("D103");
        d3.setStabilityOfProduct("5");
        d3.setWarrantyServices("6");
        d3.setBrandName("LG");
        d3.setLifeOfProduct("9");
        d3.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");

        Data d4 = new Data();
        d4.setAvg_Rating("5");
        d4.setCost("12000");
        d4.setProduct_model("D104");
        d4.setStabilityOfProduct("6");
        d4.setWarrantyServices("6");
        d3.setLifeOfProduct("6");
        d4.setBrandName("LG");
        d4.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d5 = new Data();
        d5.setAvg_Rating("9");
        d5.setCost("35000");
        d5.setProduct_model("D105");
        d5.setStabilityOfProduct("8");
        d5.setWarrantyServices("7");
        d3.setLifeOfProduct("8");
        d5.setBrandName("LG");
        d5.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");

        datas.add(d1);
        datas.add(d2);
        datas.add(d3);
        datas.add(d4);
        datas.add(d5);
    }

    public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        HashMap<String, String> hm = new HashMap<>();

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_data, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final RecyclerViewHolders holders = (RecyclerViewHolders) holder;
            holders.avgRating.setText(datas.get(position).getAvg_Rating());
            holders.brandNameTV.setText(datas.get(position).getBrandName());
            holders.modelTV.setText(datas.get(position).getProduct_model());
            holders.costTV.setText(datas.get(position).getCost());
            holders.stabTV.setText(datas.get(position).getStabilityOfProduct());
            holders.prodLifeTV.setText(datas.get(position).getLifeOfProduct());
            Picasso.with(getApplicationContext()).load(datas.get(position).getPicUrl()).into(holders.iv);
        }


        @Override
        public int getItemCount() {
            if (datas == null) {
                return 0;
            } else {
                return datas.size();
            }
        }

        public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView brandNameTV;
            TextView avgRating;
            ImageView iv;
            TextView modelTV;
            TextView costTV;
            TextView stabTV;
            TextView prodLifeTV;
            public RecyclerViewHolders(View itemView) {
                super(itemView);
                brandNameTV = (TextView) itemView.findViewById(R.id.brandNameTV);
                avgRating = (TextView)itemView.findViewById(R.id.avgRatingTV);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                modelTV = (TextView) itemView.findViewById(R.id.modelTV);
                costTV = (TextView) itemView.findViewById(R.id.costTV);
                stabTV = (TextView) itemView.findViewById(R.id.stabTV);
                prodLifeTV = (TextView) itemView.findViewById(R.id.prodLifeTV);
            }

            @Override
            public void onClick(View v) {

            }
        }

    }
}
