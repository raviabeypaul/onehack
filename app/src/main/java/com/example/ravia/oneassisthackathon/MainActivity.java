package com.example.ravia.oneassisthackathon;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.example.ravia.oneassisthackathon.Controllers.AppController;
import com.example.ravia.oneassisthackathon.Models.Data;
import com.example.ravia.oneassisthackathon.Utils.CommonUtility;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.bendik.simplerangeview.SimpleRangeView;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rv)
    RecyclerView rv;

    @BindView(R.id.popSRV)
    SimpleRangeView popSRV;

    @BindView(R.id.popTV)
    TextView popTV;

    @BindView(R.id.stabSRV)
    SimpleRangeView stabSRV;

    @BindView(R.id.stabTV)
    TextView stabTV;

    @BindView(R.id.costSRV)
    SimpleRangeView costSRV;

    @BindView(R.id.costTV1)
    TextView costTV;

    @BindView(R.id.warrSRV)
    SimpleRangeView warrSRV;

    @BindView(R.id.warrTV)
    TextView warrTV;

    @BindView(R.id.lifeSRV)
    SimpleRangeView lifeSRV;

    @BindView(R.id.lifeTV)
    TextView lifeTV;

    @BindView(R.id.bgIV)
    ImageView bgIV;

    CommonUtility cUtils;
    String TAG = getClass().getName();

    int init_val = 0;
    int init_end = 9;
    int popStart=0;
    int popEnd=9;
    int stabStart=0;
    int stabEnd=9;
    @BindView(R.id.layout)
    ViewGroup layout;

    @BindView(R.id.card)
    CardView card;

    int costStart = 0;
    int costEnd = 5;
    int warrStart=0;
    int warrEnd = 9;
    int lifeStart=0;
    int lifeEnd=9;
    ArrayList<Data> datas = new ArrayList<>();
    ArrayList<Data> finalDats = new ArrayList<>();
    LinearLayoutManager llManager;
    ProductAdapter productAdapter;
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            int sizeInDp = 24;
            int sizeInDp1 = 48;
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
            int dpAsPixels1 = (int) (sizeInDp1 * scale + 0.5f);
//            Log.e(TAG, "onCreate: dpAsPixels " + dpAsPixels);
            layout.setPadding(0, dpAsPixels, 0, 0);
            if (hasSoftKeys(getWindowManager())) {
                card.setContentPadding(0, 0, 0, dpAsPixels1);
            }

        }
        cUtils = AppController.getInstance().getcUtils();
        gson = new Gson();

        Picasso.with(getApplicationContext()).load(R.drawable.bg).placeholder(R.drawable.bg).into(bgIV);
        productAdapter = new ProductAdapter();

        llManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(llManager);
        rv.setAdapter(productAdapter);
        initData();
        popStart = stabStart = warrStart = lifeStart = init_val;
        popEnd = stabEnd = warrEnd = lifeEnd = init_end;

        popSRV.setCount(10);
        popSRV.setStart(init_val);
        popSRV.setEnd(init_end);
        stabSRV.setCount(10);
        stabSRV.setStart(init_val);
        stabSRV.setEnd(init_end);

        costSRV.setCount(6);
        costSRV.setStart(0);
        costSRV.setEnd(5);

        lifeSRV.setCount(10);
        lifeSRV.setStart(init_val);
        lifeSRV.setEnd(init_end);

        warrSRV.setCount(10);
        warrSRV.setStart(init_val);
        warrSRV.setEnd(init_end);
        popTV.setText(popStart + " - " + popEnd);
        stabTV.setText(stabStart + " - " + stabEnd);
        costStart = 0;
        costEnd = 5;
        costTV.setText((costStart * 10000) + " - " + (costEnd * 10000));
        lifeTV.setText(lifeStart + " - " + lifeEnd);
        warrTV.setText(warrStart + " - " + warrEnd);


        popSRV.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(SimpleRangeView simpleRangeView, int i) {
                popStart = i;
                popTV.setText(popStart + " - " + popEnd);
            }

            @Override
            public void onEndRangeChanged(SimpleRangeView simpleRangeView, int i) {
                popEnd = i;
                popTV.setText(popStart + " - " + popEnd);
            }
        });

        stabSRV.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(SimpleRangeView simpleRangeView, int i) {
                stabStart = i;
                stabTV.setText(stabStart + " - " + stabEnd);
            }

            @Override
            public void onEndRangeChanged(SimpleRangeView simpleRangeView, int i) {
                stabEnd = i;
                stabTV.setText(stabStart + " - " + stabEnd);
            }
        });

        costSRV.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(SimpleRangeView simpleRangeView, int i) {
                costStart = i;
                costTV.setText((costStart * 10000) + " - " + (costEnd * 10000));
            }

            @Override
            public void onEndRangeChanged(SimpleRangeView simpleRangeView, int i) {
                costEnd = i;
                costTV.setText((costStart * 10000) + " - " + (costEnd * 10000));
            }
        });

        lifeSRV.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(SimpleRangeView simpleRangeView, int i) {
                lifeStart = i;
                lifeTV.setText(lifeStart + " - " + lifeEnd);
            }

            @Override
            public void onEndRangeChanged(SimpleRangeView simpleRangeView, int i) {
                lifeEnd = i;
                lifeTV.setText(lifeStart + " - " + lifeEnd);
            }
        });

        warrSRV.setOnTrackRangeListener(new SimpleRangeView.OnTrackRangeListener() {
            @Override
            public void onStartRangeChanged(SimpleRangeView simpleRangeView, int i) {
                warrStart = i;
                warrTV.setText(warrStart + " - " + warrEnd);
            }

            @Override
            public void onEndRangeChanged(SimpleRangeView simpleRangeView, int i) {
                warrEnd = i;
                Log.e(TAG, "war onEndRangeChanged: " + i );
                warrTV.setText(warrStart + " - " + warrEnd);
            }
        });
    }

    @OnClick(R.id.fab)
    public void filter() {
        finalDats.clear();
        productAdapter.notifyDataSetChanged();
        cUtils.Loge(TAG, "filter called");
        for (int i = 0; i < datas.size(); i++) {
          if(Integer.parseInt(datas.get(i).getWarrantyServices())>= warrStart && Integer.parseInt(datas.get(i).getWarrantyServices())<= warrEnd)
          {
              cUtils.Loge(TAG, "first if");
              if(Integer.parseInt(datas.get(i).getLifeOfProduct()) >= lifeStart && Integer.parseInt(datas.get(i).getLifeOfProduct())<= lifeEnd )
              {
                  cUtils.Loge(TAG, "Second if");
                  if(Integer.parseInt(datas.get(i).getStabilityOfProduct() )>= stabStart &&  Integer.parseInt(datas.get(i).getStabilityOfProduct())<=stabEnd)
                  {
                      cUtils.Loge(TAG, "Third if");
                      if(Integer.parseInt(datas.get(i).getPopularity())>= popStart && Integer.parseInt(datas.get(i).getPopularity()) <= popEnd)
                      {
                          if(Integer.parseInt(datas.get(i).getCost())>= (costStart*10000) && Integer.parseInt(datas.get(i).getCost()) <= (costEnd*10000))
                          {
                              cUtils.Loge(TAG, "Fourth  if");
                              Data data = new Data();
                              data = datas.get(i);
                              finalDats.add(data);
                          }

                      }
                  }
              }
          }
          else
          {
              cUtils.Loge(TAG, "Warranty = " +datas.get(i).getWarrantyServices());
              cUtils.Loge(TAG, "War start = " +warrStart);
              cUtils.Loge(TAG, "War end = " +warrEnd);
          }
        }

        if(finalDats.isEmpty())
        {
            Toast.makeText(getApplicationContext(),"No Data Found under this category",Toast.LENGTH_SHORT).show();
        }
        else
        {
            Collections.sort(finalDats,COMPARE_BY_AVG);
            Collections.reverse(finalDats);
            productAdapter.notifyDataSetChanged();
        }
    }

    public boolean hasSoftKeys(WindowManager windowManager) {
        boolean hasSoftwareKeys = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display d = getWindowManager().getDefaultDisplay();

            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);

            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;

            hasSoftwareKeys = (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
        } else {
            boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            hasSoftwareKeys = !hasMenuKey && !hasBackKey;
        }
        return hasSoftwareKeys;
    }

    public void initData() {
        Data d1 = new Data();

        d1.setProduct_model("D101");
        d1.setStabilityOfProduct("7");
        d1.setLifeOfProduct("8");
        d1.setCost("35000");
        d1.setPopularity("7");
        d1.setWarrantyServices("3");
        d1.setBrandName("LG");
        Integer avg = (Integer.parseInt(d1.getPopularity())
                + Integer.parseInt(d1.getStabilityOfProduct())
                + Integer.parseInt(d1.getLifeOfProduct()) +
                (9 - Integer.parseInt(d1.getWarrantyServices())))/4;
        d1.setAvg_Rating(String.valueOf(avg));
        d1.setAvg_Rating(String.valueOf(avg));
        d1.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d2 = new Data();

        d2.setCost("22000");
        d2.setProduct_model("D102");
        d2.setStabilityOfProduct("7");
        d2.setWarrantyServices("3");
        d2.setBrandName("LG");
        d2.setLifeOfProduct("8");
        d2.setPopularity("7");
        Integer avg1 = (Integer.parseInt(d2.getPopularity())
                + Integer.parseInt(d2.getStabilityOfProduct())
                + Integer.parseInt(d2.getLifeOfProduct()) +
                (9 - Integer.parseInt(d2.getWarrantyServices())))/4;
        d2.setAvg_Rating(String.valueOf(avg1));
        d2.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d3 = new Data();

        d3.setCost("22000");
        d3.setProduct_model("D103");
        d3.setStabilityOfProduct("7");
        d3.setWarrantyServices("4");
        d3.setBrandName("LG");
        d3.setLifeOfProduct("8");
        d3.setPopularity("7");
        Integer avg3 = (Integer.parseInt(d3.getPopularity())
                + Integer.parseInt(d3.getStabilityOfProduct())
                + Integer.parseInt(d3.getLifeOfProduct()) +
                (9 - Integer.parseInt(d3.getWarrantyServices())))/4;
        d3.setAvg_Rating(String.valueOf(avg3));
        d3.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");

        Data d4 = new Data();

        d4.setCost("12000");
        d4.setProduct_model("D104");
        d4.setStabilityOfProduct("6");
        d4.setWarrantyServices("6");
        d4.setLifeOfProduct("6");
        d4.setBrandName("LG");
        d4.setPopularity("7");
        Integer avg4 = (Integer.parseInt(d4.getPopularity())
                + Integer.parseInt(d4.getStabilityOfProduct())
                + Integer.parseInt(d4.getLifeOfProduct()) +
                (9 - Integer.parseInt(d4.getWarrantyServices())))/4;
        d4.setAvg_Rating(String.valueOf(avg4));
        d4.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");


        Data d5 = new Data();
        d5.setAvg_Rating("9");
        d5.setCost("35000");
        d5.setProduct_model("D105");
        d5.setStabilityOfProduct("8");
        d5.setWarrantyServices("7");
        d5.setBrandName("LG");
        d5.setPopularity("7");
        d5.setLifeOfProduct("6");
        Integer avg5 = (Integer.parseInt(d5.getPopularity())
                + Integer.parseInt(d5.getStabilityOfProduct())
                + Integer.parseInt(d5.getLifeOfProduct()) +
                (9 - Integer.parseInt(d5.getWarrantyServices())))/4;
        d5.setAvg_Rating(String.valueOf(avg5));
        d5.setPicUrl("https://www.homedepot.com/catalog/productImages/400_compressed/e7/e73ae528-68bc-4a43-8f2c-842945d7e7fd_400_compressed.jpg");

        datas.add(d1);
        datas.add(d2);
        datas.add(d3);
        datas.add(d4);
        datas.add(d5);
//        Collections.sort(datas,COMPARE_BY_AVG);
        filter();

    }

    public class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        HashMap<String, String> hm = new HashMap<>();

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_item, parent, false);
            RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
            return rcv;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final RecyclerViewHolders holders = (RecyclerViewHolders) holder;
            holders.avgRating.setText(finalDats.get(position).getAvg_Rating());
            holders.itemNameTV.setText(finalDats.get(position).getBrandName() + " " + finalDats.get(position).getProduct_model());
            Picasso.with(getApplicationContext()).load(finalDats.get(position).getPicUrl()).into(holders.iv);
            holders.rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getApplicationContext(), ProdctDetailActivity.class);
                    String a = gson.toJson(finalDats.get(position), Data.class);
                    intent.putExtra("data", a);
                    startActivity(intent);
                }
            });
        }


        @Override
        public int getItemCount() {
            if (finalDats == null) {
                return 0;
            } else {
                return finalDats.size();
            }
        }

        public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

            TextView itemNameTV;
            TextView avgRating;
            ImageView iv;
            RelativeLayout rl;

            public RecyclerViewHolders(View itemView) {
                super(itemView);
                itemNameTV = (TextView) itemView.findViewById(R.id.itemNameTV);
                avgRating = (TextView) itemView.findViewById(R.id.avgTV);
                iv = (ImageView) itemView.findViewById(R.id.iv);
                rl = (RelativeLayout) itemView.findViewById(R.id.rl);
            }

            @Override
            public void onClick(View v) {

            }
        }

    }


    public static Comparator<Data> COMPARE_BY_AVG = new Comparator<Data>() {
        public int compare(Data one, Data other) {
            if (Integer.parseInt(one.getAvg_Rating()) > Integer.parseInt(other.getAvg_Rating())) {
                return 1;
            } else if (Integer.parseInt(one.getAvg_Rating()) == Integer.parseInt(other.getAvg_Rating())) {
                return 0;
            } else {
                return -1;
            }
        }
    };


    public static Comparator<Data> COMPARE_BY_COST = new Comparator<Data>() {
        public int compare(Data one, Data other) {
            if (Integer.parseInt(one.getCost()) > Integer.parseInt(other.getCost())) {
                return 1;
            } else if (Integer.parseInt(one.getCost()) == Integer.parseInt(other.getCost())) {
                return 0;
            } else {
                return -1;
            }
        }
    };

}
