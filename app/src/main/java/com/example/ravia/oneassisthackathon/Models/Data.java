package com.example.ravia.oneassisthackathon.Models;

/**
 * Created by ravia on 12/9/2017.
 */

public class Data {
    String Product_model;
    String Popularity;
    String StabilityOfProduct;
    String WarrantyServices;
    String Cost;
    String Avg_Rating;
    String PicUrl;
    String lifeOfProduct;
    String brandName;


    public String getLifeOfProduct() {
        return lifeOfProduct;
    }

    public void setLifeOfProduct(String lifeOfProduct) {
        this.lifeOfProduct = lifeOfProduct;
    }



    public String getPicUrl() {
        return PicUrl;
    }

    public void setPicUrl(String picUrl) {
        PicUrl = picUrl;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }



    public String getProduct_model() {
        return Product_model;
    }

    public void setProduct_model(String product_model) {
        Product_model = product_model;
    }

    public String getPopularity() {
        return Popularity;
    }

    public void setPopularity(String popularity) {
        Popularity = popularity;
    }

    public String getStabilityOfProduct() {
        return StabilityOfProduct;
    }

    public void setStabilityOfProduct(String stabilityOfProduct) {
        StabilityOfProduct = stabilityOfProduct;
    }

    public String getWarrantyServices() {
        return WarrantyServices;
    }

    public void setWarrantyServices(String warrantyServices) {
        WarrantyServices = warrantyServices;
    }

    public String getCost() {
        return Cost;
    }

    public void setCost(String cost) {
        Cost = cost;
    }

    public String getAvg_Rating() {
        return Avg_Rating;
    }

    public void setAvg_Rating(String avg_Rating) {
        Avg_Rating = avg_Rating;
    }


}
