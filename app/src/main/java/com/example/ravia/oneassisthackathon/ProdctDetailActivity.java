package com.example.ravia.oneassisthackathon;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ravia.oneassisthackathon.Models.Data;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import at.grabner.circleprogress.CircleProgressView;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Amit on 12/9/2017.
 */

public class ProdctDetailActivity extends AppCompatActivity {
    private ImageView mProductImage;
    private TextView mProductPrice;
    private CircleProgressView mProgressPopularty, mProgressStability, mProgressWarranty, mProgressProductLife;
    Data data;
    Gson gson;
    ImageView bgIV;

    @BindView(R.id.layout)
    ViewGroup layout;



    @BindView(R.id.card)
    CardView card;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_item_list);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            int sizeInDp = 24;
            int sizeInDp1 = 48;
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (sizeInDp * scale + 0.5f);
            int dpAsPixels1 = (int) (sizeInDp1 * scale + 0.5f);
//            Log.e(TAG, "onCreate: dpAsPixels " + dpAsPixels);
            layout.setPadding(0, dpAsPixels, 0, 0);
            if (hasSoftKeys(getWindowManager())) {
                card.setContentPadding(0, 0, 0, dpAsPixels1);
            }

        }
        gson = new Gson();
        data = new Data();
        data = gson.fromJson(getIntent().getStringExtra("data"), Data.class);
        findViewId();
    }


    private void findViewId() {
        bgIV = (ImageView) findViewById(R.id.bgIV);
        Picasso.with(getApplicationContext()).load(R.drawable.bg).into(bgIV);
        mProductImage = (ImageView) findViewById(R.id.image_product);
        mProductPrice = (TextView) findViewById(R.id.tv_product_price);
        mProgressPopularty = (CircleProgressView) findViewById(R.id.progress_popularty);
        mProgressStability = (CircleProgressView) findViewById(R.id.progres_stability);
        mProgressWarranty = (CircleProgressView) findViewById(R.id.progress_warranty);
        mProgressProductLife = (CircleProgressView) findViewById(R.id.progress_product_life);
        mProductPrice.setText(data.getCost());
        mProgressPopularty.setValue( Float.valueOf(data.getPopularity())/10);
        mProgressStability.setValue( Float.valueOf(data.getStabilityOfProduct())/10);
        mProgressWarranty.setValue( Float.valueOf(data.getWarrantyServices())/10);
        mProgressProductLife.setValue( Float.valueOf(data.getLifeOfProduct())/10);
        mProgressProductLife.setMaxValue(9);
        mProgressProductLife.setValue(.3f);
        loadImage();
    }

    public boolean hasSoftKeys(WindowManager windowManager) {
        boolean hasSoftwareKeys = true;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Display d = getWindowManager().getDefaultDisplay();

            DisplayMetrics realDisplayMetrics = new DisplayMetrics();
            d.getRealMetrics(realDisplayMetrics);

            int realHeight = realDisplayMetrics.heightPixels;
            int realWidth = realDisplayMetrics.widthPixels;

            DisplayMetrics displayMetrics = new DisplayMetrics();
            d.getMetrics(displayMetrics);

            int displayHeight = displayMetrics.heightPixels;
            int displayWidth = displayMetrics.widthPixels;

            hasSoftwareKeys = (realWidth - displayWidth) > 0 || (realHeight - displayHeight) > 0;
        } else {
            boolean hasMenuKey = ViewConfiguration.get(getApplicationContext()).hasPermanentMenuKey();
            boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
            hasSoftwareKeys = !hasMenuKey && !hasBackKey;
        }
        return hasSoftwareKeys;
    }

    private void loadImage() {
        Picasso.with(this)
                .load(data.getPicUrl())
                .placeholder(R.mipmap.ic_launcher)   // optional
                .into(mProductImage);
    }


}
