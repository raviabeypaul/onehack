package com.example.ravia.oneassisthackathon.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by ravi on 2/3/16.
 */
public class FileUtility {
    String folderName;
    ByteArrayOutputStream byteArrayOutputStream;

    public Boolean create_folder(String folderName)
    {
        this.folderName = folderName;
        String path = Environment.getExternalStorageDirectory().toString();
//        commonUtility.callLog("GalerySelectFragment","path = ",path);
        File folder = new File(Environment.getExternalStorageDirectory() + "/"+folderName);
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        if (success) {
            return true;
        } else {
            return false;

        }
    }



    public  void saveInFolderOfApllication(Context context, String appName, String fileName, Bitmap bmp , int compressRate)
    {
        CommonUtility commonUtility = new CommonUtility();

        File destination = new File(Environment.getExternalStorageDirectory().getAbsolutePath(),folderName+"/"+ System.currentTimeMillis() + fileName +".jpg");
        FileOutputStream fo;
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();

        bmp.compress(Bitmap.CompressFormat.JPEG,compressRate,bytes);
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            Toast.makeText(context,"Image Saved And Compressed", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public ByteArrayOutputStream compressAndSaveInByteArrayOutputStream(Bitmap bitmap)
    {
        this.byteArrayOutputStream = getByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        return byteArrayOutputStream;
    }

    public ByteArrayOutputStream getByteArrayOutputStream()
    {
        byteArrayOutputStream = new ByteArrayOutputStream();
        return byteArrayOutputStream;
    }

    public String getStringImage(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = compressAndSaveInByteArrayOutputStream(bitmap);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        String encodedImage  = Base64.encodeToString(imageBytes , Base64.DEFAULT);
        return encodedImage;
    }
    private void resizeImage(Context context, String imagepath, int compressRate, String fileName) {
        CommonUtility commonUtility = new CommonUtility();
        Bitmap bitmap = BitmapFactory.decodeFile(imagepath);


        FileOutputStream fileOutputStream = null;


        String folderName = "Throwback/";
        FileOutputStream out = null;

        File file = new File(fileName + ".jpeg");

//        commonUtility.Loge("File Utility : ", "File Exists" + file.toString(), " : Bitmap = " + fileName);
        FileUtility fileUtility = new FileUtility();
        fileUtility.create_folder(folderName);
        String path = Environment.getExternalStorageDirectory().toString() + "/" + folderName;
        fileUtility.saveInFolderOfApllication(context, folderName, fileName, bitmap, compressRate);
        fileUtility.compressAndSaveInByteArrayOutputStream(bitmap);

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


}
