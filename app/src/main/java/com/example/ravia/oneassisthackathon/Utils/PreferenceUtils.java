package com.example.ravia.oneassisthackathon.Utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

/**
 * Created by ravi on 22/2/16.
 */
public class PreferenceUtils {
    SharedPreferences sharedPreferences;
    String sharedPreferenceName;
    SharedPreferences.Editor editor;
    Context context;
    KeyStore keyStore;
    String TAG = getClass().getName();
    String pass = "my_key";
    public PreferenceUtils(Context context, String sharedPreferenceName)
    {
        this.context = context;
        this.sharedPreferenceName = sharedPreferenceName;
        sharedPreferences = context.getSharedPreferences(this.sharedPreferenceName, Context.MODE_PRIVATE);
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
            keyStore.load(null);
            createNewKeys();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void initShPref(Context context, String sharedPreferenceName) {
        this.context = context;
        this.sharedPreferenceName = sharedPreferenceName;
        sharedPreferences = context.getSharedPreferences(this.sharedPreferenceName, Context.MODE_PRIVATE);
    }


    public void setString(String key, String value) {
//        encryptString(firebaseToken,value);
        editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.commit();
    }

    public void setLong(String Key, Long Value) {
        editor = sharedPreferences.edit();
        editor.putLong(Key, Value);
        editor.commit();
    }

    public void setBoolean(String key, Boolean value) {
        editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getBoolean(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, null);
//        return decryptString(ksey);
    }

    public String getStringWithoutEncryption(String key) {
        return sharedPreferences.getString(key, null);
//        return decryptString(firebaseToken);
    }

    public Long getLong(String key) {
        return sharedPreferences.getLong(key, 0);
    }

    public int getInt(String key) {
        return sharedPreferences.getInt(key, 0);
    }

    public Boolean isKeyPresent(String key) {
        return sharedPreferenceName.contains(key);
    }

    public void setInt(String Key, int value) {
        editor = sharedPreferences.edit();
        editor.putInt(Key, value);
        editor.commit();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public void createNewKeys() {
        String alias = pass;
        try {
            // Create new firebaseToken if needed
            if (!keyStore.containsAlias(alias)) {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, 1);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                        .setAlias(alias)
                        .setSubject(new X500Principal("CN=Sample Name, O=Android Authority"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
                generator.initialize(spec);
                KeyPair keyPair = generator.generateKeyPair();
            }
        } catch (Exception e) {
//            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.e(TAG, Log.getStackTraceString(e));
        }
//        refreshKeys();
    }

    public void encryptString( String k, String v) {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(pass, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();
            String initialText = k;
            Cipher inCipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);
            ByteArrayOutputStream outputStreamV = new ByteArrayOutputStream();
            CipherOutputStream cipherOutputStreamV = new CipherOutputStream(
                    outputStreamV, inCipher);

            cipherOutputStreamV.write(v.getBytes("UTF-8"));
                cipherOutputStreamV.close();

            byte [] vals1 = outputStreamV.toByteArray();
            String final_v = Base64.encodeToString(vals1, Base64.DEFAULT);
            Log.e(TAG, "encryptString: value " +  final_v + "---"  );
            editor = sharedPreferences.edit();
            editor.putString(k, final_v);
            editor.commit();
        } catch (Exception e) {
//            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    public String   decryptString( String alias) {
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(pass, null);
            RSAPrivateKey privateKey = (RSAPrivateKey) privateKeyEntry.getPrivateKey();

            Cipher outputK = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            outputK.init(Cipher.DECRYPT_MODE, privateKey);

            String cipherTextK = alias;
            CipherInputStream cipherInputStreamK = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(cipherTextK, Base64.DEFAULT)), outputK);
            ArrayList<Byte> valuesK = new ArrayList<>();
            int nextByteK;
            while ((nextByteK = cipherInputStreamK.read()) != -1) {
                valuesK.add((byte)nextByteK);
            }
            byte[] bytesK = new byte[valuesK.size()];
            for(int i = 0; i < bytesK.length; i++) {
                bytesK[i] = valuesK.get(i).byteValue();
            }
            String finalK = new String(bytesK, 0, bytesK.length, "UTF-8");
            Log.e(TAG, "decryptString: finalK : " + finalK );
            Cipher outputV = Cipher.getInstance("RSA/ECB/PKCS1Padding", "AndroidOpenSSL");
            outputK.init(Cipher.DECRYPT_MODE, privateKey);

            String cipherTextV = getStringWithoutEncryption(alias);
            Log.e(TAG, "decryptString: cipherText value" + cipherTextV );
            CipherInputStream cipherInputStreamV = new CipherInputStream(
                    new ByteArrayInputStream(Base64.decode(cipherTextV, Base64.DEFAULT)), outputV);
            ArrayList<Byte> valuesV = new ArrayList<>();
            int nextByteV;
            while ((nextByteV = cipherInputStreamV.read()) != -1) {
                valuesV.add((byte)nextByteV);
            }

            byte[] bytesV = new byte[valuesV.size()];
            for(int i = 0; i < bytesV.length; i++) {
                bytesV[i] = valuesV.get(i).byteValue();
            }
            String finalV = new String(bytesV, 0, bytesV.length, "UTF-8");

            Log.e(TAG, "decryptString:  firebaseToken" + finalK );
            Log.e(TAG, "decryptString:  value" + finalV );
            return finalK;

        } catch (Exception e) {

//            Toast.makeText(this, "Exception " + e.getMessage() + " occured", Toast.LENGTH_LONG).show();
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

}
